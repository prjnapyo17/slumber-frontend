import {
  MenuOutlined,
  DownOutlined,
  CaretDownOutlined,
} from "@ant-design/icons";
import { Button, Dropdown, Menu, message } from "antd";
import React, { useState } from "react";

const menu = (
  <Menu
    items={[
      {
        label: <a href="">Edit Profile</a>,
        key: "0",
      },
      {
        type: "divider",
      },
      {
        label: "Logout",
        key: "1",
      },
    ]}
  />
);

const MyHeader = (props) => {
  return (
    <div className="drop-shadow-lg mx-6">
      <div className="text-lg h-16 flex items-center px-8">
        <MenuOutlined />
        <div className="flex justify-between w-full items-center">
          <span className="ml-10">{props.title}</span>

          <Dropdown
            overlay={menu}
            trigger={["click"]}
            overlayStyle={{ color: "white", background: "Black" }}
          >
            <a onClick={(e) => e.preventDefault()}>
              <Button
                className=" text-white py-4 px-5 rounded-3xl w-56 space-x-32 flex items-center"
                style={{
                  background: "#131F43",
                }}
              >
                <span className="text-base">Guest</span>
                <CaretDownOutlined />
              </Button>
            </a>
          </Dropdown>
        </div>
      </div>
    </div>
  );
};

export default MyHeader;
