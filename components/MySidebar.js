import {
  LaptopOutlined,
  NotificationOutlined,
  UserOutlined,
  DashboardOutlined,
  PieChartOutlined,
  DesktopOutlined,
  TeamOutlined,
  FileOutlined,
  PlayCircleOutlined,
  AppstoreOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu } from "antd";
import "antd/dist/antd.css";
import Link from "next/link";
import React, { useState } from "react";

const { Sider } = Layout;

const getItem = (label, key, icon, children) => {
  return {
    key,
    icon,
    children,
    label,
  };
};

const items1 = [
  getItem(<Link href="/dashboard">Dashboard</Link>, "1", <AppstoreOutlined />),
  getItem(<Link href="/dashboard/media">Media</Link>, "2", <DesktopOutlined />),
  getItem("Customer", "3", <UserOutlined />),
  getItem("Playlist", "sub1", <PlayCircleOutlined />, [
    getItem("Master Playlist", "4"),
    getItem("Sub Playlist", "5"),
  ]),
  getItem("Creator", "6", <FileOutlined />),
];

const MySidebar = () => {
  return (
    <div className="h-full shadow-md">
      <div className="logo h-16 bg-white text-center p-4 font-bold text-xl">
        Slumber
      </div>
      <Menu
        defaultSelectedKeys={["1"]}
        mode="inline"
        items={items1}
        style={{
          height: "100%",

          borderRight: 0,
        }}
      />
    </div>
  );
};

export default MySidebar;
