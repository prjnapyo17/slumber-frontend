import Footer from "../components/Footer";
import Header from "../components/Header";
import Hero from "../components/Hero";
import App from "../components/Test";

export default function Home() {
  return (
    <div>
      <App />
      <Header />
      <Hero />
      <Footer />
    </div>
  );
}
