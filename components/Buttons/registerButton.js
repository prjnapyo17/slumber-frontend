import React from "react";

class RegisterButton extends React.Component {
  render() {
    return (
      <div>
        <button className="rounded bg-primary p-2 font-bold text-putih hover:bg-slate-500 w-72">
          {this.props.name}
        </button>
      </div>
    );
  }
}
export default RegisterButton;
