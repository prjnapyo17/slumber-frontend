import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import MyHeader from "../components/MyHeader";
import MySidebar from "../components/MySidebar";
import React, { useState } from "react";
import { Layout } from "antd";
import MyContent from "../components/MyContent";

const { Header, Sider, Content } = Layout;
const Dashboard = ({ title, children }) => {
  return (
    <div>
      <Layout>
        <Sider>
          <MySidebar />
        </Sider>
        <Layout>
          <Header className="bg-white shadow-sm p-0">
            <MyHeader title={title} />
          </Header>
          <Content>{children}</Content>
        </Layout>
      </Layout>
    </div>
  );
};

export default Dashboard;
