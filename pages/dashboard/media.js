import MyTable from "../../components/MyTable";
import Dashboard from "../../layouts/dashboard";

const Media = () => {
  return (
    <div>
      <MyTable />
    </div>
  );
};

export default Media;

Media.getLayout = function Layout(page) {
  return <Dashboard title="Media">{page}</Dashboard>;
};
