import {
  DeleteOutlined,
  EditOutlined,
  InfoCircleOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Table, Input, Space, Button, Popconfirm } from "antd";
import React from "react";

const { Search } = Input;

const columns = [
  {
    title: "Name Media",
    dataIndex: "name",
    sorter: true,
  },
  {
    title: "Album",
    dataIndex: "album",
    sorter: true,
    defaultSortOrder: "ascend",
  },
  {
    title: "Creator",
    dataIndex: "creator",
    sorter: true,
  },
  {
    title: "Action",
    dataIndex: "",
    render: () => (
      <Space size="small">
        <a>
          <InfoCircleOutlined className="text-lg" />
        </a>
        <a>
          <EditOutlined className="text-yellow-500 text-lg" />
        </a>
        <Popconfirm
          title="Sure to delete?"
          onConfirm={() => handleDelete(record.key)}
        >
          <a>
            <DeleteOutlined className="text-red-500 text-lg" />
          </a>
        </Popconfirm>
      </Space>
    ),
  },
];
const data = [
  {
    key: "1",
    name: "John Brown",
    album: "Tole",
    creator: "Jaja Miharja",
  },
  {
    key: "2",
    name: "Jim Green",
    album: "Maman",
    creator: "Sri Sulaswati",
  },
  {
    key: "3",
    name: "Joe Black",
    album: "Ampas",
    creator: "Maman Abdurrahman",
  },
  {
    key: "4",
    name: "Jim Red",
    album: "Kumarori",
    creator: "Hariono",
  },
];

const onChange = (pagination, filters, sorter, extra) => {
  console.log("params", pagination, filters, sorter, extra);
};

const onSearch = (value) => console.log(value);

const MyTable = () => (
  <div className="py-4 px-16">
    <Search style={{ width: 300 }} />
    <Table
      columns={columns}
      dataSource={data}
      onChange={onChange}
      style={{ margin: "15px 0px" }}
    />
  </div>
);

export default MyTable;
