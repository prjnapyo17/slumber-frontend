import {
  DesktopOutlined,
  PlayCircleOutlined,
  UserOutlined,
} from "@ant-design/icons";

const MyContent = () => {
  return (
    <div className="totalData py-4 px-16">
      <div className="h-72 flex py-4 space-x-5">
        <div className="w-96 bg-white rounded-2xl drop-shadow-lg">
          <div className="top-0 border-8 border-red-500 rounded-t-lg"></div>
          <div className="p-8">
            <UserOutlined className="text-6xl" />
            <p className="mt-12 text-3xl font-bold">12.000 </p>
            <p className="-mt-5 text-base font-semibold">Total Customer</p>
          </div>
        </div>
        <div className="w-96 bg-white rounded-2xl drop-shadow-lg">
          <div className="top-0 border-8 border-blue-500 rounded-t-lg"></div>
          <div className="p-8">
            <DesktopOutlined className="text-6xl" />
            <p className="mt-12 text-3xl font-bold">12</p>
            <p className="-mt-5 text-base font-semibold">Total Playlist</p>
          </div>
        </div>
        <div className="w-96 bg-white rounded-xl drop-shadow-lg">
          <div className="top-0 border-8 border-green-500 rounded-t-lg"></div>
          <div className="p-8">
            <PlayCircleOutlined className="text-6xl" />
            <p className="mt-12 text-3xl font-bold">12.354</p>
            <p className="-mt-5 text-base font-semibold">Total Media</p>
          </div>
        </div>
        <div className="w-96 bg-white rounded-xl drop-shadow-lg">
          <div className="top-0 border-8 border-yellow-500 rounded-t-lg"></div>
          <div className="p-8">
            <UserOutlined className="text-6xl" />
            <p className="mt-12 text-3xl font-bold">534</p>
            <p className="-mt-5 text-base font-semibold">Total Creator</p>
          </div>
        </div>
      </div>

      <div className="flex space-x-7">
        <div className="totalPlay w-6/12 bg-white p-4 rounded-2xl drop-shadow-lg">
          <span className="font-bold text-lg">Top Play Media</span>
          <table className="mt-2 w-full table-auto">
            <thead className="border-b-2 text-left">
              <tr className="text-lg">
                <th>#</th>
                <th>Media</th>
                <th>Creator</th>
                <th>Play</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>Jaja Miharja</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>woeowe weokowkoew</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td className="">Mangust Mat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat </td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
            </tbody>
          </table>
        </div>

        <div className="totalPlay w-6/12 bg-white float-right p-4 rounded-2xl drop-shadow-lg">
          <span className="font-bold text-lg">Top Favorite Media</span>
          <table className="mt-2 w-full">
            <thead className="border-b-2 text-left">
              <tr className="text-lg">
                <th>#</th>
                <th>Media</th>
                <th>Creator</th>
                <th>Favorite</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>Jaja Miharja</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>woeowe weokowkoew</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>Mangsut Amat</td>
                <td>woeowe weokowkoew</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangust Mat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
              <tr>
                <td>
                  <img src="/login.png" className="w-10" />
                </td>
                <td>woeowe weokowkoew</td>
                <td>Mangsut Amat</td>
                <td>3.000.000</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default MyContent;
