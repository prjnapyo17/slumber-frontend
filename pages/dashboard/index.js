import MyContent from "../../components/MyContent";
import Dashboard from "../../layouts/dashboard";

const Index = () => {
  return (
    <div>
      <MyContent />
    </div>
  );
};

export default Index;

Index.getLayout = function Layout(page) {
  return <Dashboard title="Dashboard">{page}</Dashboard>;
};
